# REPOSITORY MOVED

## This repository has moved to

https://github.com/ignition-tooling/gazebodistro

## Issues and pull requests are backed up at

https://osrf-migration.github.io/ignition-tooling-gh-pages/#!/osrf/gazebodistro

## Until BitBucket removes Mercurial support, this read-only mercurial repository will be at

https://bitbucket.org/osrf-migrated/gazebodistro

## More info at

https://community.gazebosim.org/t/important-gazebo-and-ignition-are-going-to-github/533

